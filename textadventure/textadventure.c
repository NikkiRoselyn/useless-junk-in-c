#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef struct characters {
  char first_name[11];
  char last_name[11];
  int age;
} characters;

void your_adventure_beginning()
{
  int choices;

  struct characters *your_character = malloc(sizeof(characters));

  printf("What is your name?_beginning\nName: ");
  scanf("%10s %10s", your_character -> first_name, your_character -> last_name);

  printf("What is your age?\nAge: ");
  scanf("%d", &your_character -> age);
  
  printf("You wake up in a world you're not familiar with. No one is human, they are all anthropomorphic animals.");
  printf("what do you do?\n(Input a number between 1-3 for a choice:\n1. Get help | 2. Explore | 3. Loot for equipment sneakily): ");
  scanf("%d", &choices);
  
  switch (choices) {
    case 1:
      goto help;
      break;
    case 2:
      goto explore;
      break;
    case 3:
      goto loot;
      break;
    default:
      printf("You do nothing and die.");
      break;
  }

  /* Help choice outcome */
help:
  printf("You look for help.\n");
  printf("You find a village and you head over to it to find help.\n");
  printf("You find a two anthropomorphic fox girls who can help you.\n");


  /* Explore choice outcome */
explore:
  printf("You explore the forest you woke up in, there is a village nearby.\n");
  printf("You find some gold, which apparently is the currency in this world.\n");


  /* Loot choice outcome */
loot:
  printf("You go and loot for things, sneakily. You manage to loot a sword, some gold and some armour.\n");

  free(your_character);
}

int main(void)
{
  int play_or_quit;

  printf("Do you want to play the text adventure?(1 for yes | 0 for no)\nAnswer: ");
  scanf("%d", &play_or_quit);

  switch (play_or_quit) {
    case 1: //True AKA Yes
      your_adventure_beginning();
      break;
    case 0: //False AKA No
      printf("Hope you want to play next time!\n");
      break;
  }
}
